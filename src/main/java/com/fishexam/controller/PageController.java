package com.fishexam.controller;

import com.fishexam.pojo.MessagePojo;
import com.fishexam.pojo.OperationPojo;
import com.fishexam.pojo.PetsUserPojo;
import com.fishexam.service.MessageService;
import com.fishexam.service.OperationService;
import com.fishexam.service.PetsUserService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class PageController {
    @Autowired
    PetsUserService petsUserService;


    @Autowired
    MessageService messageService;

    @Autowired
    OperationService operationService;


    @RequestMapping({"/login", "/login.html", "/"})
    public String login() {
        return "login/login";
    }

    //主页
    @RequestMapping("/index")
    public String goToHome(Model model, Integer page) {
        if (page == null) {
            page = 1;
        }
        PageInfo<PetsUserPojo> petsUserPojoPageInfo = petsUserService.selectPets(page);
        List<MessagePojo> messagePojos = messageService.selectMessage();
        System.out.println("messagePojos.size()=====>" + messagePojos.size());
        //获取未读消息 messageStatusSize的个数
        int messageStatusSize = 0;
        for (MessagePojo messagePojo : messagePojos) {
            if (messagePojo == null) continue;
            int messageStatus = messagePojo.getMessageStatus();
            if (messageStatus == 0) {
                messageStatusSize++;
            }
        }
        String messageStatusSizes = messageStatusSize + "条未读消息";
        model.addAttribute("messageStatusSize", messageStatusSizes);
        model.addAttribute("msgs", messagePojos);
        model.addAttribute("pets", petsUserPojoPageInfo);
        model.addAttribute("page", page);
        return "index";
    }

    @RequestMapping("/indexs")
    public String goToHomes(Model model, Integer page) {
        if (page == null) {
            page = 1;
        }
        PageInfo<PetsUserPojo> petsUserPojoPageInfo = petsUserService.selectPets(page);
        model.addAttribute("info", petsUserPojoPageInfo);
        System.out.println(petsUserPojoPageInfo);
        return "indexs";
    }

    //手术排班信息
    @RequestMapping("/doctorBed")
    public String doctorBed(Model model) {
        List<MessagePojo> messagePojos = messageService.selectMessage();
        List<OperationPojo> operation = operationService.selectOperation();

        //获取未读消息 messageStatusSize的个数
        int messageStatusSize = 0;
        for (MessagePojo messagePojo : messagePojos) {
            int messageStatus = messagePojo.getMessageStatus();
            if (messageStatus == 0) {
                messageStatusSize++;
            }
        }
        String messageStatusSizes = messageStatusSize + "条未读消息";
        model.addAttribute("operation", operation);
        model.addAttribute("messageStatusSize", messageStatusSizes);
        model.addAttribute("msgs", messagePojos);
        return "home/doctorBed";
    }

    //添加新问诊宠物
    @RequestMapping("/addPets")
    public String addPets(Model model) {
        List<MessagePojo> messagePojos = messageService.selectMessage();

        //获取未读消息 messageStatusSize的个数
        int messageStatusSize = 0;
        for (MessagePojo messagePojo : messagePojos) {
            int messageStatus = messagePojo.getMessageStatus();
            if (messageStatus == 0) {
                messageStatusSize++;
            }
        }
        String messageStatusSizes = messageStatusSize + "条未读消息";
        model.addAttribute("messageStatusSize", messageStatusSizes);
        model.addAttribute("msgs", messagePojos);
        return "addPets";
    }

    @RequestMapping("/toOperation")
    public String toOperation(Model model) {
        List<MessagePojo> messagePojos = messageService.selectMessage();
        //获取未读消息 messageStatusSize的个数
        int messageStatusSize = 0;
        for (MessagePojo messagePojo : messagePojos) {
            int messageStatus = messagePojo.getMessageStatus();
            if (messageStatus == 0) {
                messageStatusSize++;
            }
        }
        String messageStatusSizes = messageStatusSize + "条未读消息";
        model.addAttribute("messageStatusSize", messageStatusSizes);
        model.addAttribute("msgs", messagePojos);
        return "home/addOperation";
    }


    @RequestMapping("/personChat")
    public String chat() {
        return "home/chat";
    }

    @RequestMapping("/webSocket")
    public String webSocket() {
        return "WebSocket";
    }
}
