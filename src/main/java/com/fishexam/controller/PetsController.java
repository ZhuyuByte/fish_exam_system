package com.fishexam.controller;

import com.fishexam.pojo.MessagePojo;
import com.fishexam.pojo.PetsUserPojo;
import com.fishexam.service.MessageService;
import com.fishexam.service.PetsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @program: FishExam
 * @description: 宠物管理controller层
 * @author Zhuyu 1766722033@qq.com
 * 
 * @since 2020-05-24 20:35
 **/
@Controller
public class PetsController {
    @Autowired
    PetsUserService petsUserService;

    @RequestMapping("/date")
    public String selectDate(int date, Model model){
        System.out.println(date);
        List<PetsUserPojo> petsUserPojos = petsUserService.selectPetsByDate(date);
        model.addAttribute("pets",petsUserPojos);
        return "index";
    }

    @RequestMapping("/savePets")
    public String savePets(String number,String name,String names,String age,String status,String daterangepicker,String gridRadios,String bed){
        int gender = 1;
        int beds = 1;
        if ("2".equals(gridRadios)){
            gender=2;
        }
        if (bed!=null){
            beds=0;
        }

        int i = petsUserService.insterIntoPet(number, name, names, age, status, daterangepicker, gender, beds);
        System.out.println(number+name+names+age+status+daterangepicker+gender+beds);
        System.out.println(i);
        return "redirect:/index";
    }


}
