## 1. 下载或clone代码到本地
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019192204-b3e4ef7c-9a6f-4f84-93f1-4d91d4f1b833.png)
下载解压后的项目如下所示:
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019249522-5ba395f5-4021-484d-9d01-c840611f46df.png)

## 2. 将数据库文件导入,这里使用的数据库管理工具是Navicat
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019315819-cb2d6159-2fbc-4743-9a8e-095bcf46ff25.png)
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019379283-696ec256-9ff4-4a9d-a436-6b08d27e6dc5.png)
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019411985-0b99f15f-6fec-494d-9d19-85b1e66533f7.png)
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019466360-e4334dcf-d889-4c1d-8166-fea047d984d0.png)

## 3. 启动项目,用IDE打开项目

设置maven仓库地址
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019663547-1a679906-ca53-4f45-a746-ed7d56a4e954.png)
根据实际情况配置
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019677089-a311d393-8891-485b-98c2-e905856ba10c.png)

## 4. 修改数据库配置信息
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019725292-683cd4f0-888e-494f-93fd-0a64f364f032.png)
## 5. 启动项目
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019774329-e7671214-c7a4-46c2-acff-4815388b8854.png)

启动成功:
![image.png](https://cdn.nlark.com/yuque/0/2021/png/1528966/1639019807347-8903631d-8da7-46c3-96d7-1bd68db9774c.png)

## 启动成功后,浏览器访问: http://localhost/toLogin

账号：
admin
123456